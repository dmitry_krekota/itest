<?php

use Illuminate\Database\Seeder;

use Saas\Builder;

/**
 * @RUN php artisan db:seed --class=TestsTableSeeder
 * @RUN php artisan migrate:refresh --seed
 */
class TestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tests = [
            [
                'name' => 'Оцінка управлінських якостей',
                'questions' => json_encode(json_decode(file_get_contents(storage_path() . "/json/msai.json")), JSON_UNESCAPED_UNICODE),
                'type' => 'MSAI'
            ]
        ];

        foreach($tests as $table) {
            $record = App\Test::firstOrNew([
                'name' => $table['name']
            ]);
            if (!$record->exists) {
                $record->fill([
                    'questions' => $table['questions'],
                    'type' => $table['type']
                ]);
                $record->save();
                echo "Test {$record->name} created\n";
            } else {
                echo "Test {$record->name} already exists\n";
            }
        }
    }
}