<?php

use Illuminate\Database\Seeder;

use Saas\Builder;

/**
 * @RUN php artisan db:seed --class=UsersTableSeeder
 * @RUN php artisan migrate:refresh --seed
 */
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Dmitry Krekota',
                'email' => 'dmitry.krekota@gmail.com',
            ],
            [
                'name' => 'Demo User',
                'email' => 'demo.user@demo.com',
            ],
            [
                'name' => 'Мещанінов Олександр',
                'email' => 'vrector@chmnu.edu.ua',
            ]
        ];

        /**
         * @var App\User $record
         */
        foreach($users as $user) {
            $record = App\User::firstOrNew([
                'name' => $user['name'],
            ]);
            if (!$record->exists) {
                $record->fill([
                    'email' => $user['email'],
                    'password' => bcrypt('123456'),
                ]);
                $record->save();
                echo "User {$record->name} created\n";
            } else {
                echo "User {$record->name} already exists\n";
            }
        }
    }
}