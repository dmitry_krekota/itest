<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'questions', 'type'
    ];

    public function passedTests()
    {
        return $this->hasMany('App\PassedTest');
    }
}
