<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Auth;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    public function resetForm()
    {
        if (Auth::guest()) {
            return redirect('auth/login');
        }
        return view('auth/reset');
    }

    public function customReset(Request $request)
    {
        if ($request->input('password') == $request->input('password_confirmation')) {
            $user = Auth::user();
            if ($user->email != 'demo.user@demo.com') {
                $user->password = bcrypt($request->input('password'));
                $user->save();
                return redirect('auth/logout');
            } else {
                return view('auth/reset');
            }
        } else {
            return view('auth/reset');
        }
    }
}
