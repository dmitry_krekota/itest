<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', ['uses' => 'HomeController@index']);

Route::get('/auth/login', ['uses' => 'Auth\LoginController@loginForm']);

Route::post('/auth/login', ['uses' => 'Auth\LoginController@login']);

Route::get('/auth/logout', ['uses' => 'Auth\LoginController@logout']);

Route::get('/auth/reset', ['uses' => 'Auth\ResetPasswordController@resetForm']);

Route::post('/auth/reset', ['uses' => 'Auth\ResetPasswordController@customReset']);

Route::get('/tests/all', function () {
    return App\Test::get([
        'id', 'name', 'questions', 'type', 'created_at'
    ]);
});

Route::get('/passed_tests/all', function () {
    return App\PassedTest::get([
        'id', 'user_id', 'test_id', 'answers', 'created_at'
    ]);
});

Route::get('/users/all', function () {
    return App\User::get([
        'id', 'name'
    ]);
});

Route::get('tests/id/{id}', function ($id) {
    return App\Test::where('id', $id)->get([
        'id', 'name', 'questions', 'type', 'created_at'
    ])[0];
});

Route::get('passed_tests/id/{id}', function ($id) {
    return App\PassedTest::where('id', $id)->get([
        'id', 'user_id', 'test_id', 'answers'
    ])[0];
});

Route::post('tests/answers', function () {
    $record = new App\PassedTest([
        'answers' => json_encode(Request::input('answers'), JSON_UNESCAPED_UNICODE)
    ]);
    $user = App\User::where('id', Auth::user()->id)->get()[0];
    $record->user()->associate($user);
    $test = App\Test::where('id', Request::input('testId'))->get()[0];
    $record->test()->associate($test);
    $record->save();
    return Response::json(array('success' => true, 'last_insert_id' => $record->id), 200);
});

Route::post('tests/recommends', function () {
   App\PassedTest::where('id', Request::input('passedTestId'))->update(array('answers' => json_encode(Request::input('answers'), JSON_UNESCAPED_UNICODE)));
    return Response::json(array('success' => true, 200));
});