const jsBuilder = require('systemjs-builder');
const htmlMin = require('html-minifier');

const gulp = require('gulp');
const concat = require('gulp-concat');
const typescript = require('gulp-typescript');
const inlineNg2Template = require('gulp-inline-ng2-template');
const sass = require('gulp-sass');

function minifyTemplate(path, ext, file, cb) {
    try {
        let minifiedFile = htmlMin.minify(file, {
            collapseWhitespace: true,
            caseSensitive: true,
            removeComments: true,
            removeRedundantAttributes: true
        });
        cb(null, minifiedFile);
    }
    catch (err) {
        cb(err);
    }
}

gulp.task('js', function(done) {
    let typescriptProject = typescript.createProject('./tsconfig.json');
    typescriptProject.src()
        .pipe(inlineNg2Template({
            base: './resources/assets/',
            useRelativePaths: true,
            templateProcessor: minifyTemplate
        }))
        .pipe(typescriptProject())
        .pipe(gulp.dest('temp/itest-app'))
        .on('end', function() {
            new jsBuilder('/', 'systemjs.config.js')
                .buildStatic('temp/itest-app/main.js', 'public/js/itest-app.js', {
                    minify: false,
                    globalDefs: {DEBUG: false}
                })
                .then(function() {
                    done();
                })
                .catch(function(error) {
                    console.log('error', error);
                    done();
                });
        });
});

gulp.task('css', function(done) {
    gulp.src('./resources/assets/**/*.scss')
        .pipe(sass.sync().on('error', function() {
            sass.logError();
            done();
        }))
        .pipe(gulp.dest('./temp/assets'))
        .on('end', function() {
            gulp.src([
                'node_modules/bootstrap/dist/css/bootstrap.min.css',
                'node_modules/@angular/material/core/theming/prebuilt/indigo-pink.css',
                'temp/assets/**/*.css'
            ])
                .pipe(concat('itest-app.css'))
                .pipe(gulp.dest('public/css'));
            done();
        });
});

gulp.task('cssmin', function() {
	let cssmin = require('gulp-cssmin');
	gulp.src('public/css/itest-app.css')
		.pipe(cssmin())
		.pipe(gulp.dest('public/css'));
});

gulp.task('jsmin', function() {
	let jsmin = require('gulp-jsmin');
	gulp.src('public/js/itest-app.js')
		.pipe(jsmin())
		.pipe(gulp.dest('public/js'));
});