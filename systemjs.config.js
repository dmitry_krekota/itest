﻿(function() {
    // map tells the System loader where to look for things
    let map = {
        'core-js': './node_modules/core-js',
        'zone.js': './node_modules/zone.js',
        '@angular': './node_modules/@angular',
        'rxjs': './node_modules/rxjs',
        'hammerjs': './node_modules/hammerjs/hammer.js'
    };
    // packages tells the System loader how to load when no filename and/or no extension
    let packages = {};
    let ngPackageNames = [
        'common',
        'compiler',
        'core',
        'forms',
        'http',
        'platform-browser',
        'platform-browser-dynamic',
        'upgrade',
        'router',
        'material'
    ];
    let meta = {};

    // Individual files (~300 requests):
    function packIndex(pkgName) {
        packages['@angular/' + pkgName] = {main: 'index.js', defaultExtension: 'js'};
    }

    // Bundled (~40 requests):
    function packUmd(pkgName) {
        packages['@angular/' + pkgName] = {main: 'bundles/' + pkgName + '.umd.js', defaultExtension: 'js'};
    }

    // Most environments should use UMD; some (Karma) need the individual index files
    let setPackageConfig = System.packageWithIndex ? packIndex : packUmd;
    // Add package entries for angular packages
    ngPackageNames.forEach(setPackageConfig);
    let config = {
        meta: meta,
        map: map,
        packages: packages,
        defaultJSExtensions: true
    };
    System.config(config);
})();