<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ URL::asset('favicon.png') }}" type="image/x-icon">
    <title>{{ config('app.name') }}</title>
    @yield('head-bottom')
</head>
@yield('body')
</html>
