@extends('layouts.index')

@section('head-bottom')
    <link rel="stylesheet" href="{{ URL::asset('css/itest-app.css') }}">
@endsection

@section('body')
<body class="it-login">
    <div class="container it-login">
        <form class="form-signin" method="POST" action="{{ url('auth/login') }}">
            {{ csrf_field() }}
            <h2 class="form-signin-heading">Вхід до системи</h2>
            <label for="inputEmail" class="sr-only">Email адреса</label>
            <input type="email" id="inputEmail" class="form-control" placeholder="Email адреса" name="email" value="demo.user@demo.com" required autofocus>
            <label for="inputPassword" class="sr-only">Пароль</label>
            <input type="password" id="inputPassword" class="form-control" placeholder="Пароль" name="password" value="123456" required>
            @if ($errors->has('email'))<span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>@endif
            @if ($errors->has('password'))<span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>@endif
            <button class="btn btn-lg btn-primary btn-block" type="submit">Увійти</button>
        </form>
    </div>
</body>
@endsection
