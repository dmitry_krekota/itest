@extends('layouts.index')

@section('head-bottom')
    <link rel="stylesheet" href="{{ URL::asset('css/itest-app.css') }}">
@endsection

@section('body')
    <body class="it-login">
    <div class="container it-login">
        <form class="form-signin" method="POST" action="{{ url('/auth/reset') }}">
            {{ csrf_field() }}
            <h2 class="form-signin-heading">Зміна паролю</h2>
            <label for="inputEmail" class="sr-only">Пароль</label>
            <input id="password" type="password" class="form-control" placeholder="Пароль" name="password" required>
            <label for="inputPassword" class="sr-only">Підтвердіть пароль</label>
            <input id="password-confirm" type="password" class="form-control" placeholder="Підтвердіть пароль" name="password_confirmation" required>
            @if ($errors->has('password'))<span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>@endif
            @if ($errors->has('password_confirmation'))<span class="help-block"><strong>{{ $errors->first('password_confirmation') }}</strong></span>@endif
            <button class="btn btn-lg btn-primary btn-block" type="submit">Змінити пароль</button>
        </form>
    </div>
    </body>
@endsection