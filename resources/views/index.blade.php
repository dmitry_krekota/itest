@extends('layouts.index')

@section('head-bottom')
    <link rel="stylesheet" href="{{ URL::asset('css/itest-app.css') }}">
@endsection

@section('body')
    <body>
    <nav class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#">iTest</a>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/') }}">Головна</a>
                </li>
            </ul>
            <form class="form-inline mt-2 mt-md-0">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/auth/reset') }}">Зміна паролю</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/auth/logout') }}">Вихід</a>
                    </li>
                </ul>
            </form>
        </div>
    </nav>
    <div class="container">
        <it-root>Загрузка...</it-root>
    </div>
    <footer class="footer">
        <div class="container">
            <span class="text-muted">Copyright © 2017 iTest. Система для тестування.</span>
        </div>
    </footer>
    <script src="{{ URL::asset('js/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('js/highcharts.js') }}"></script>
    <script src="{{ URL::asset('js/highcharts-more.js') }}"></script>
    <script src="{{ URL::asset('js/itest-app.js') }}"></script>
    </body>
@endsection