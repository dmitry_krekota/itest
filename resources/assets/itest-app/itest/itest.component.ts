import {Component} from '@angular/core';

@Component({
    selector: 'it-root',
    templateUrl: 'itest.component.html'
})
export class ITestComponent {
    title = 'ITestComponent works!';
}
