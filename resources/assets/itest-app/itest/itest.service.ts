import {Injectable} from '@angular/core';

@Injectable()
class ITestService {
    public static apiUrl = './';
}

export {ITestService};