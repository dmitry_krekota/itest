import {TestsComponent} from './components/tests/tests.component';
import {TestComponent} from  './components/test/test.component';
import {AnswersComponent} from  './components/answers/answers.component';
import {PassedTestComponent} from './components/passed_test/passed_test.component';

export const ITestRouting = [
    {path: '', component: TestsComponent},
    {path: 'test/:id', component: TestComponent},
    {path: 'passed_test/:id', component: PassedTestComponent},
    {path: 'answers', component: AnswersComponent}
];