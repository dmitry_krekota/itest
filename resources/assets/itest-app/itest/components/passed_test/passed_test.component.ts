import {Component} from '@angular/core';
import {PassedTestService} from './passed_test.service';
import {ActivatedRoute} from '@angular/router';
import {TestService} from '../test/test.service';

@Component({
    selector: 'it-tests',
    templateUrl: './passed_test.component.html'
})

class PassedTestComponent {
    passedTest: any;
    resultsCurrent: any = {};
    dominantCurrent: string;
    dominantPrefer: string;
    dominantPerspective: string;
    resultsPerspective: any = {};
    resultsPrefer: any = {};
    recommends: any = [];
    cultureName: any = {
        a: "(A) Адхократична, синергічна модель розвитку",
        b: "(B) Кланова, гнучка модель розвитку",
        c: "(C) Ієрархічна, консервативна модель розвитку",
        d: "(D) Ринкова, експансивна модель розвитку"
    };
    offerRecommendsMode: boolean = true;

    constructor(private route: ActivatedRoute, private passedTestService: PassedTestService, private testService: TestService) {
    }

    static getViewData(results) {
        return [null, results.a, null, results.d, null, results.c, null, results.b];
    }

    toggleRecommends(index) {
        let i;
        for (i = 0; i < this.recommends.length; i++) {
            if (index == i) {
                this.recommends[i]._opened = !this.recommends[i]._opened;
            } else {
                this.recommends[i]._opened = false;
            }
        }
    }

    saveRecommends() {
        this.passedTestService.sendRecommends(this.passedTest.id, this.passedTest.answers).subscribe(data => {
            this.offerRecommendsMode = false;
        });
    }

    updatePerspectiveData() {
        console.log('this.resultsPerspective', this.resultsPerspective);
        window['$']('.answer-view-container').highcharts().series[2].setData(PassedTestComponent.getViewData(this.resultsPerspective));
        this.dominantPerspective = PassedTestComponent.getDominant(this.resultsPerspective);
    }

    setRecommends(recommend, type, i) {
        let indexInRecommends, coefficient = 0.03;
        recommend['_checked' + type.toUpperCase() + i] = !recommend['_checked' + type.toUpperCase() + i];
        indexInRecommends = this.passedTest.answers.recommends.indexOf(recommend[type][i]);
        if (indexInRecommends < 0) {
            this.passedTest.answers.recommends.push(recommend[type][i]);
            switch (type) {
                case 'a':
                    this.resultsPerspective[type] += coefficient;
                    this.resultsPerspective['c'] -= coefficient;
                    break;
                case 'c':
                    this.resultsPerspective[type] += coefficient;
                    this.resultsPerspective['a'] -= coefficient;
                    break;
                case 'b':
                    this.resultsPerspective[type] += coefficient;
                    this.resultsPerspective['d'] -= coefficient;
                    break;
                case 'd':
                    this.resultsPerspective[type] += coefficient;
                    this.resultsPerspective['b'] -= coefficient;
                    break;
            }
        } else {
            this.passedTest.answers.recommends.splice(indexInRecommends, 1);
            switch (type) {
                case 'a':
                    this.resultsPerspective[type] -= coefficient;
                    this.resultsPerspective['c'] += coefficient;
                    break;
                case 'c':
                    this.resultsPerspective[type] -= coefficient;
                    this.resultsPerspective['a'] += coefficient;
                    break;
                case 'b':
                    this.resultsPerspective[type] -= coefficient;
                    this.resultsPerspective['d'] += coefficient;
                    break;
                case 'd':
                    this.resultsPerspective[type] -= coefficient;
                    this.resultsPerspective['b'] += coefficient;
                    break;
            }
        }
        this.resultsPerspective.a = Math.round(this.resultsPerspective.a * 100) / 100;
        this.resultsPerspective.b = Math.round(this.resultsPerspective.b * 100) / 100;
        this.resultsPerspective.c = Math.round(this.resultsPerspective.c * 100) / 100;
        this.resultsPerspective.d = Math.round(this.resultsPerspective.d * 100) / 100;
        this.passedTest.answers.recommendPerspective = this.resultsPerspective;
        this.updatePerspectiveData();
    }

    static getDominant(result) {
        if (result.a > result.b && result.a > result.c && result.a > result.d) {
            return 'a';
        }
        if (result.b > result.c && result.b > result.d) {
            return 'b';
        }
        if (result.c > result.d) {
            return 'c';
        }
        return 'd';
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.passedTestService.getPassedTest(params['id']).subscribe(data => {
                let i, aSum, bSum, cSum, dSum;
                this.passedTest = data;
                this.passedTest.answers = JSON.parse(this.passedTest.answers);

                if (!this.passedTest.answers.recommends) {
                    this.passedTest.answers.recommends = [];
                } else {
                    this.offerRecommendsMode = false;
                }

                this.testService.getTest(this.passedTest['test_id']).subscribe(data => {
                    this.recommends = JSON.parse(data.questions).recommends;
                    console.log('this.recommends', this.recommends);
                });

                // set current:
                aSum = 0;
                bSum = 0;
                cSum = 0;
                dSum = 0;
                for (i = 0; i < this.passedTest.answers.current.length; i++) {
                    aSum += this.passedTest.answers.current[i]['a'];
                    bSum += this.passedTest.answers.current[i]['b'];
                    cSum += this.passedTest.answers.current[i]['c'];
                    dSum += this.passedTest.answers.current[i]['d'];
                }
                this.resultsCurrent.a = Math.round(aSum / this.passedTest.answers.current.length) / 100;
                this.resultsCurrent.b = Math.round(bSum / this.passedTest.answers.current.length) / 100;
                this.resultsCurrent.c = Math.round(cSum / this.passedTest.answers.current.length) / 100;
                this.resultsCurrent.d = Math.round(dSum / this.passedTest.answers.current.length) / 100;
                this.dominantCurrent = PassedTestComponent.getDominant(this.resultsCurrent);

                // set prefer:
                aSum = 0;
                bSum = 0;
                cSum = 0;
                dSum = 0;
                for (i = 0; i < this.passedTest.answers.prefer.length; i++) {
                    aSum += this.passedTest.answers.prefer[i]['a'];
                    bSum += this.passedTest.answers.prefer[i]['b'];
                    cSum += this.passedTest.answers.prefer[i]['c'];
                    dSum += this.passedTest.answers.prefer[i]['d'];
                }
                this.resultsPrefer.a = Math.round(aSum / this.passedTest.answers.prefer.length) / 100;
                this.resultsPrefer.b = Math.round(bSum / this.passedTest.answers.prefer.length) / 100;
                this.resultsPrefer.c = Math.round(cSum / this.passedTest.answers.prefer.length) / 100;
                this.resultsPrefer.d = Math.round(dSum / this.passedTest.answers.prefer.length) / 100;
                this.dominantPrefer = PassedTestComponent.getDominant(this.resultsPrefer);

                // set perspective:
                if (this.passedTest.answers.recommendPerspective) {
                    this.resultsPerspective = this.passedTest.answers.recommendPerspective;
                } else {
                    aSum = 0;
                    bSum = 0;
                    cSum = 0;
                    dSum = 0;
                    for (i = 0; i < this.passedTest.answers.perspective.length; i++) {
                        aSum += this.passedTest.answers.perspective[i]['a'];
                        bSum += this.passedTest.answers.perspective[i]['b'];
                        cSum += this.passedTest.answers.perspective[i]['c'];
                        dSum += this.passedTest.answers.perspective[i]['d'];
                    }
                    this.resultsPerspective.a = Math.round(aSum / this.passedTest.answers.perspective.length) / 100;
                    this.resultsPerspective.b = Math.round(bSum / this.passedTest.answers.perspective.length) / 100;
                    this.resultsPerspective.c = Math.round(cSum / this.passedTest.answers.perspective.length) / 100;
                    this.resultsPerspective.d = Math.round(dSum / this.passedTest.answers.perspective.length) / 100;
                }

                window['$']('.answer-view-container').highcharts({
                    chart: {
                        polar: true,
                        type: 'area'
                    },
                    title: false,
                    credits: {
                        enabled: false
                    },
                    pane: {
                        startAngle: 0
                    },
                    xAxis: {
                        categories: ['Гнучкість', this.cultureName['a'], 'Зовнішній', this.cultureName['d'], 'Контроль', this.cultureName['c'], 'Внутрішній', this.cultureName['b']],
                        tickmarkPlacement: 'on',
                        lineColor: '#3f51b5',
                        lineWidth: 1,
                        gridLineColor: '#3f51b5'
                    },
                    yAxis: {
                        min: 0,
                        max: 1,
                        tickPositions: [0, 0.33, 0.67, 1],
                        gridLineColor: '#3f51b5'
                    },
                    tooltip: {
                        shared: true
                    },
                    series: [{
                        type: 'area',
                        name: 'Зараз',
                        connectEnds: true,
                        connectNulls: true,
                        fillOpacity: 0.15,
                        pointPlacement: 'on',
                        color: '#13f61f',
                        marker: {
                            symbol: 'circle'
                        },
                        data: PassedTestComponent.getViewData(this.resultsCurrent)
                    }, {
                        type: 'area',
                        name: 'Бажано',
                        connectEnds: true,
                        connectNulls: true,
                        color: '#a0660c',
                        fillOpacity: 0.15,
                        pointPlacement: 'on',
                        marker: {
                            symbol: 'circle'
                        },
                        data: PassedTestComponent.getViewData(this.resultsPrefer)
                    }, {
                        type: 'area',
                        name: 'Перспектива',
                        connectEnds: true,
                        connectNulls: true,
                        color: '#000000',
                        fillOpacity: 0.15,
                        pointPlacement: 'on',
                        marker: {
                            symbol: 'circle'
                        }
                    }]
                });
                this.updatePerspectiveData();
            });
        });
    }
}
export {PassedTestComponent};