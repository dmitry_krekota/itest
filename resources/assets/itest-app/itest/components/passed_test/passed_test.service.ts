import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import {ITestService} from '../../itest.service';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';

@Injectable()
class PassedTestService {

    constructor(private http: Http) {
    }

    getPassedTest(id) {
        return this.http.get(ITestService.apiUrl + 'passed_tests/id/' + id)
            .map((res: Response) => res.json());
    }

    sendRecommends(passedTestId, answers) {
        let body = JSON.stringify({
                passedTestId: passedTestId,
                answers: answers
            }),
            headers = new Headers({
                'Content-Type': 'application/json;charset=utf-8'
            });

        return this.http.post(ITestService.apiUrl + 'tests/recommends', body, {headers: headers})
            .map((resp: Response) => resp.json());
    }
}

export {PassedTestService};