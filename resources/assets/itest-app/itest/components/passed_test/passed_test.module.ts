import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {MaterialModule} from '@angular/material';
import { FormsModule } from '@angular/forms';

import {PassedTestService} from './passed_test.service';
import {PassedTestComponent} from './passed_test.component';

@NgModule({
    imports: [
        BrowserModule,
        RouterModule,
        MaterialModule,
        FormsModule
    ],
    declarations: [
        PassedTestComponent
    ],
    providers: [
        PassedTestService
    ],
    exports: [
        PassedTestComponent
    ]
})
class PassedTestModule {
}
export {PassedTestModule}


