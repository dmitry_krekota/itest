import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {ITestService} from '../../itest.service';
import 'rxjs/add/operator/map';

@Injectable()
class TestsService {

    constructor(private http: Http) {
    }

    getTests() {
        return this.http.get(ITestService.apiUrl + 'tests/all')
            .map((res: Response) => res.json());
    }
}

export {TestsService};