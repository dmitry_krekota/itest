import {Component} from '@angular/core';
import {TestsService} from './tests.service';

@Component({
    selector: 'it-tests',
    templateUrl: './tests.component.html'
})

class TestsComponent {
    tests: any = [];

    constructor(private testsService: TestsService) {
        this.testsService.getTests().subscribe(data => {
            this.tests = data;
        });
    }
}
export {TestsComponent};