import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {MaterialModule} from '@angular/material';

import {TestsService} from './tests.service';
import {TestsComponent} from './tests.component';

@NgModule({
    imports: [
        BrowserModule,
        MaterialModule,
        RouterModule
    ],
    declarations: [
        TestsComponent
    ],
    providers: [
        TestsService
    ],
    exports: [
        TestsComponent
    ]
})
class TestsModule {
}
export {TestsModule}


