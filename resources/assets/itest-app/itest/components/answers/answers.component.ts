import {Component} from '@angular/core';
import {AnswersService} from './answers.service';

@Component({
    selector: 'it-tests',
    templateUrl: './answers.component.html'
})

class AnswersComponent {
    passedTests: any = [];
    users: any = [];

    getUserNameById(id) {
        let i;
        for (i = 0; i < this.users.length; i++) {
            if (id == this.users[i].id) {
                return this.users[i].name;
            }
        }
    }

    constructor(private testsService: AnswersService) {
        this.testsService.getPassedTests().subscribe(data => {
            this.passedTests = data;
        });

        this.testsService.getUsers().subscribe(data => {
            this.users = data;
        });
    }
}
export {AnswersComponent};