import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {ITestService} from '../../itest.service';
import 'rxjs/add/operator/map';

@Injectable()
class AnswersService {

    constructor(private http: Http) {
    }

    getPassedTests() {
        return this.http.get(ITestService.apiUrl + '/passed_tests/all')
            .map((res: Response) => res.json());
    }

    getUsers() {
        return this.http.get(ITestService.apiUrl + '/users/all')
            .map((res: Response) => res.json());
    }
}

export {AnswersService};