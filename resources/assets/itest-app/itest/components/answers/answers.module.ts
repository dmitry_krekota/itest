import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {MaterialModule} from '@angular/material';

import {AnswersService} from './answers.service';
import {AnswersComponent} from './answers.component';

@NgModule({
    imports: [
        BrowserModule,
        RouterModule,
        MaterialModule
    ],
    declarations: [
        AnswersComponent
    ],
    providers: [
        AnswersService
    ],
    exports: [
        AnswersComponent
    ]
})
class AnswersModule {
}
export {AnswersModule}


