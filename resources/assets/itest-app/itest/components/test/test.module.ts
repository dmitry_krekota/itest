import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {MaterialModule} from '@angular/material';

import {TestService} from './test.service';
import {TestComponent} from './test.component';

@NgModule({
    imports: [
        BrowserModule,
        MaterialModule
    ],
    declarations: [
        TestComponent
    ],
    providers: [
        TestService
    ],
    exports: [
        TestComponent
    ]
})
class TestModule {
}
export {TestModule}


