import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import {ITestService} from '../../itest.service';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';

export class Test {
    popular_posts: number = 0;
    trending_posts: number = 0;
    total_posts: number = 0;
}

@Injectable()
class TestService {
    constructor(private http: Http) {
    }

    getTest(id) {
        return this.http.get(ITestService.apiUrl + 'tests/id/' + id)
            .map((res: Response) => res.json());
    }

    sendAnswers(testId, answers) {
        let body = JSON.stringify({
                testId: testId,
                answers: answers
            }),
            headers = new Headers({
                'Content-Type': 'application/json;charset=utf-8'
            });

        return this.http.post(ITestService.apiUrl + 'tests/answers', body, {headers: headers})
            .map((resp: Response) => resp.json());
    }
}

export {TestService};