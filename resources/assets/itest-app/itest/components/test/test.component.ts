import {Component} from '@angular/core';
import {TestService} from './test.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'it-tests',
    templateUrl: './test.component.html'
})

class TestComponent {
    test: any;
    questions: any;
    questionsType: string;
    answers: any;
    testId: any;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private testService: TestService) {
    }

    setAnswers(event, answerType, answerId, letter1, letter2) {
        this.answers[answerType][answerId][letter1] = event.value;
        this.answers[answerType][answerId][letter2] = 100 - event.value;
    }

    sendAnswers() {
        this.testService.sendAnswers(this.testId, this.answers).subscribe(data => {
            if (data) {
                console.log(data);
                this.router.navigate(['/passed_test',data.last_insert_id]);
            }
        })
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe(params => {
            this.testService.getTest(params['id']).subscribe(data => {
                let i;
                this.test = data;
                this.testId = data.id;
                this.questions = JSON.parse(this.test.questions);
                this.questionsType = this.test.type.toLowerCase();
                this.answers = {
                    current: [],
                    prefer: [],
                    perspective: [],
                };
                for (i = 0; i < this.questions[this.questionsType].length; i++) {
                    this.answers.current.push({a: 50, b: 50, c: 50, d: 50});
                    this.answers.prefer.push({a: 50, b: 50, c: 50, d: 50});
                    this.answers.perspective.push({a: 50, b: 50, c: 50, d: 50});
                }
            });
        });
    }
}
export {TestComponent};