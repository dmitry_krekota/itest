import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {MaterialModule} from '@angular/material';
import {TestsModule} from './components/tests/tests.module';
import {TestModule} from './components/test/test.module';
import {AnswersModule} from './components/answers/answers.module';
import {PassedTestModule} from './components/passed_test/passed_test.module';
import {RouterModule} from '@angular/router';

import {ITestRouting} from './itest.routing';

import {ITestComponent} from './itest.component';

@NgModule({
    declarations: [
        ITestComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        MaterialModule.forRoot(),
        TestsModule,
        TestModule,
        AnswersModule, //TODO-guru PassedTestsModule (priority 0)
        PassedTestModule,
        RouterModule.forRoot(ITestRouting, {
            useHash: true
        })
    ],
    providers: [],
    bootstrap: [ITestComponent]
})
export class ITestModule {
}
