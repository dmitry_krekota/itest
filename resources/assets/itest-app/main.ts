import 'core-js/es7/reflect';
import 'zone.js/dist/zone';
import 'hammerjs';

import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {enableProdMode} from '@angular/core';
import {environment} from './environments/environment';
import {ITestModule} from './itest/itest.module';

if (environment.production) {
    enableProdMode();
}

platformBrowserDynamic().bootstrapModule(ITestModule);
